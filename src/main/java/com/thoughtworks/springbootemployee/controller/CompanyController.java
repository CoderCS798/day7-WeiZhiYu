package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private static final List<Company> companies = new ArrayList<>();
    private static final AtomicLong atomicCompanyId = new AtomicLong(0);

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        company.setId(atomicCompanyId.incrementAndGet());
        companies.add(company);
        return company;
    }

    @GetMapping
    public List<Company> getCompanies() {
        return companies;
    }

    @GetMapping("/{companyId}")
    public Company getCompanyById(@PathVariable Long companyId) {
        return companies.stream()
                .filter(company -> companyId.equals(company.getId()))
                .findFirst()
                .orElse(null);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesOfCompany(@PathVariable Long companyId) {
        return EmployeeRepository.getEmployees().stream()
                .filter(employee -> companyId.equals(employee.getCompanyId()))
                .collect(Collectors.toList());
    }

    @PutMapping("/{companyId}")
    public Company updateCompany(@PathVariable Long companyId, @RequestBody Company company) {
        Company updatedCompany = getCompanyById(companyId);
        updatedCompany.setName(company.getName());
        return updatedCompany;
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long companyId) {
        companies.remove(getCompanyById(companyId));
    }

}
