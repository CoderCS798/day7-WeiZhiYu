package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee) {
        return EmployeeRepository.addEmployee(employee);
    }

    @GetMapping
    public List<Employee> getEmployees() {
        return EmployeeRepository.getEmployees();
    }

    @GetMapping("/{employeeId}")
    public Employee getEmployeeById(@PathVariable Long employeeId) {
        return EmployeeRepository.getEmployeeById(employeeId);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return EmployeeRepository.getEmployeesByGender(gender);
    }

    @PutMapping("/{employeeId}")
    public Employee updateAgeAndSalary(@PathVariable Long employeeId, @RequestBody Employee employee) {
        return EmployeeRepository.updateAgeAndSalary(employeeId, employee);
    }

    @DeleteMapping("/{employeeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long employeeId) {
        EmployeeRepository.deleteEmployeeById(employeeId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> queryEmployees(@RequestParam Integer page, @RequestParam Integer size) {
        return EmployeeRepository.queryEmployees(page, size);
    }

}
